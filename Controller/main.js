const electron = require("electron");
const url = require('url');
const path = require('path');

// const env = {
//     baseUrl: process.env.baseUrl
// }

// const env = require("../environment/environment");
// console.log(env.environment.apiUrl);

const fs = require('fs');
const env = path.join(__dirname, '../environment/environment.js');
let data_env;
fs.readFile(env, (err, data) => {

	// if there's an error, log it and return
	if (err) {
		console.error(err)
		return
	}

    console.log("cek nilai environment : ");

    data_env = data.toString();
    console.log(data_env);
    let arr = data_env.split("\n");
    console.log("data baris pertama : ", arr[1]);
})

const {app, BrowserWindow, Menu, ipcMain} = electron;

let mainWindow;
let loginWindow;
let loketWindow;
let dirPath = "../View/";

// Listen for app to be ready
app.on('ready' , function(){
    // Create new Window
    mainWindow = new BrowserWindow({});
    //Load HTML file into window
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, dirPath+'mainWindow.html'),
        protocol: 'file:',
        slashes: true
    }));

    // Quit app when close
    mainWindow.on('closed', function(){
        app.quit();
    })

    // Build menu from template
    const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
    // Insert Menu
    Menu.setApplicationMenu(mainMenu);

    //maximize Window
    mainWindow.maximize();
}); 

// Handle create login Window
function createLoginWindow(){
    // Create new Window
    loginWindow = new BrowserWindow({
        // width: 500,
        // height: 300,
        title: 'Login Account',
        webPreferences: {
            nodeIntegration: true
        }
    });
    
    //Maximize screen login
    loginWindow.maximize();
    
    //Load HTML file into window
    loginWindow.loadURL(url.format({
        pathname: path.join(__dirname, dirPath+'loginWindow.html'),
        protocol: 'file:',
        slashes: true
    }));

    // Garbage Collection
    loginWindow.on('closed', function(){
        loginWindow = null;
    })
}

// Handle create Loket Window
function createLoketWindow(){
    // Create new Window
    loketWindow = new BrowserWindow({
        // width: 500,
        // height: 300,
        title: 'Loket',
        webPreferences: {
            nodeIntegration: true
        }
    });
    
    //Maximize screen loket
    loketWindow.maximize();
    
    //Load HTML file into window
    loketWindow.loadURL(url.format({
        pathname: path.join(__dirname, dirPath+'loketWindow.html'),
        protocol: 'file:',
        slashes: true
    }));

    // Garbage Collection
    loketWindow.on('closed', function(){
        loketWindow = null;
    })
}

// Handle submit data for Validation
ipcMain.on('submit:data', function(e, data){
    console.log("call ipcMain submit main.js");
    
    console.log("Passing data : ", data.username, data.password);

    // send Token or IP address if success login (change this after fetch success)
    loginWindow.webContents.send('token', token ='someToken');
});


// const historyService = require('../Service/historyService');
// ipcMain.on('get:history', function(e, status){
//     console.log("ipcMain status get History : ", status);
//     historyService.getAllHistory();
// });

// Create Menu Template
const mainMenuTemplate = [
    {
        label: 'File',
        submenu: [
            {
                label: 'Login',
                click(){
                    createLoginWindow();
                }
            },
            {
                label: 'Loket',
                click(){
                    createLoketWindow();
                }
            },
            {
                label: 'Quit',
                accelerator: process.platform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
                click(){
                    app.quit();
                }
            }

        ]
    }
];

// If mac, add empty object in first menu
if(process.platform == 'darwin'){
    mainMenuTemplate.unshift({});
} 

// Add developer tools item if not in production
if(process.env.NODE_ENV !== 'production'){
    mainMenuTemplate.push({
        label: 'Developer Tools',
        submenu: [
            {
                label: 'Toggle DevTools',
                accelerator: process.platform == 'darwin' ? 'Command+I' : 'Ctrl+I',
                click(item, focusedWindow){
                    focusedWindow.toggleDevTools();
                }
            },
            {
                role: 'reload'
            }
        ]
    });
}