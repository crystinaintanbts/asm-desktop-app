module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        'Montserrat': ['Montserrat', 'sans-serif']
      },
      backgroundImage: {
        'pattern-login': "url('assets/truck.svg')"
      },
      backgroundColor: {
        'button': '#FA6400',
        'base-dark-blue': 'rgb(0,0,0)',
        'blue-ocean': '#075FDA',
        'dark-blue-ocean': '#043B8A',
        'dark' : '#053780'
      },
      padding: {
        '4': '4px',
      },
      gradientColorStops: {
          'base': 'rgb(0,0,0,0.2)',
          'blue': '#075FDA',
          'dark': '#043B8A'
      },
      textColor: {
        'white': '#FFFFFF',
        'orange': '#FA6400'
      }
      // radialGradientColors:{
      //   'dark-blue': ['#043B8A', '#075FDA'],
      // },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    // require('tailwindcss-gradients'), 
  ],
}
